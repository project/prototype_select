<?php

namespace Drupal\prototype_select\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Extension\ThemeHandler;

/**
 * Implements a PrototypeSelectConfig form.
 */
class PrototypeSelectConfigForm extends ConfigFormBase {

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Theme handler.
   *
   * @var \Drupal\Core\Extension\ThemeHandler
   *   Theme handler.
   */
  protected $themeHandler;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, ThemeHandler $themeHandler, MessengerInterface $messenger) {
    parent::__construct($config_factory);
    $this->themeHandler = $themeHandler;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('theme_handler'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'prototype_select_config_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['prototype_select.settings'];
  }

  /**
   * PrototypeSelect configuration form.
   *
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    // PrototypeSelect settings:
    $prototype_select_conf = $this->configFactory->get('prototype_select.settings');

    $form['search_enable_threshold'] = [
      '#type' => 'select',
      '#title' => $this->t('Minimum number to show Search on Selects'),
      '#options' => array_merge(['0' => $this->t('Disabled')], ['1' => $this->t('Always Enabled')], range(2, 25)),
      '#default_value' => $prototype_select_conf->get('search_enable_threshold'),
      '#description' => $this->t('The minimum number of options to apply Prototype Select search box. Example : choosing 10 will only apply Prototype Select search if the number of options is greater or equal to 10.'),
    ];

    $form['max_shown_results'] = [
      '#type' => 'number',
      '#title' => $this->t('Maximum shown results'),
      '#default_value' => $prototype_select_conf->get('max_shown_results'),
      '#description' => $this->t('Only show the first (n) matching options in the results. This can be used to increase performance for selects with very many options. Leave blank to show all results.'),
    ];

    $form['selector'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Apply PrototypeSelect to the following elements'),
      '#description' => $this->t('A comma-separated list of valid CSS selectors to apply Prototype Select to, such as <code>select#edit-operation, select#edit-type</code> or <code>.prototype_select-select</code>. Defaults to <code>select</code> to apply Prototype Select to all <code>&lt;select&gt;</code> elements.'),
      '#default_value' => $prototype_select_conf->get('selector') ? $prototype_select_conf->get('selector') : 'select[data-select-a11y]',
    ];

    $form['exclude_selector'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Exclude PrototypeSelect from the following elements'),
      '#description' => $this->t('A comma-separated list of valid CSS selectors to exclude Prototype Select from. These elements will be filtered out of the elments that match the selector option above.'),
      '#default_value' => $prototype_select_conf->get('exclude_selector') ? $prototype_select_conf->get('exclude_selector') : '',
    ];

    $form['options'] = [
      '#type' => 'details',
      '#title' => $this->t('PrototypeSelect general options'),
      '#open' => TRUE,
    ];

    $form['options']['search_contains'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Search also in the middle of words'),
      '#default_value' => $prototype_select_conf->get('search_contains'),
      '#description' => $this->t('Per default prototype_select searches only at beginning of words. Enable this option will also find results in the middle of words.
      Example: Search for <em>land</em> will also find <code>Switzer<strong>land</strong></code>.'),
    ];

    $form['options']['show_selected'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show selected values'),
      '#default_value' => $prototype_select_conf->get('show_selected'),
      '#description' => $this->t('Show selected value chips when multiple values are allowed.'),
    ];

    $form['options']['prototype_select_include'] = [
      '#type' => 'radios',
      '#title' => $this->t('Use for admin pages and/or front end pages'),
      '#options' => [
        PROTOTYPE_SELECT_INCLUDE_EVERYWHERE => $this->t('Include on every page'),
        PROTOTYPE_SELECT_INCLUDE_ADMIN => $this->t('Include only on admin pages'),
        PROTOTYPE_SELECT_INCLUDE_NO_ADMIN => $this->t('Include only on front end pages'),
      ],
      '#default_value' => $prototype_select_conf->get('prototype_select_include'),
    ];

    $form['strings'] = [
      '#type' => 'details',
      '#title' => $this->t('String overrides'),
      '#open' => TRUE,
    ];

    $form['strings']['text_help'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Use tab (or the down arrow key) to navigate through the list of suggestions'),
      '#required' => TRUE,
      '#default_value' => $prototype_select_conf->get('text_help'),
    ];

    $form['strings']['text_placeholder'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Search in the list'),
      '#required' => TRUE,
      '#default_value' => $prototype_select_conf->get('text_placeholder'),
    ];

    $form['strings']['text_no_result'] = [
      '#type' => 'textfield',
      '#title' => $this->t('No result'),
      '#required' => TRUE,
      '#default_value' => $prototype_select_conf->get('text_no_result'),
    ];

    $form['strings']['text_results'] = [
      '#type' => 'textfield',
      '#title' => $this->t('{x} suggestion(s) available'),
      '#required' => TRUE,
      '#default_value' => $prototype_select_conf->get('text_results'),
    ];

    $form['strings']['text_delete_item'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Remove {t}'),
      '#required' => TRUE,
      '#default_value' => $prototype_select_conf->get('text_delete_item'),
    ];

    $form['strings']['text_delete'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Remove'),
      '#required' => TRUE,
      '#default_value' => $prototype_select_conf->get('text_delete'),
    ];

    $form['strings']['text_button'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Multi-select Button'),
      '#required' => TRUE,
      '#description' => $this->t("This is the text that prompts the user to choose options for a multi-select element. &lt;label&gt; will pull from the select element's label."),
      '#default_value' => $prototype_select_conf->get('text_button'),
    ];
    return $form;
  }

  /**
   * PrototypeSelect configuration form submit handler.
   *
   * Validates submission by checking for duplicate entries, invalid
   * characters, and that there is an abbreviation and phrase pair.
   *
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory->getEditable('prototype_select.settings');

    $config
      ->set('search_enable_threshold', $form_state->getValue('search_enable_threshold'))
      ->set('max_shown_results', $form_state->getValue('max_shown_results'))
      ->set('selector', $form_state->getValue('selector'))
      ->set('exclude_selector', $form_state->getValue('exclude_selector'))
      ->set('search_contains', $form_state->getValue('search_contains'))
      ->set('show_selected', $form_state->getValue('show_selected'))
      ->set('disabled_themes', $form_state->getValue('disabled_themes'))
      ->set('prototype_select_include', $form_state->getValue('prototype_select_include'))
      ->set('text_help', $form_state->getValue('text_help'))
      ->set('text_placeholder', $form_state->getValue('text_placeholder'))
      ->set('text_no_result', $form_state->getValue('text_no_result'))
      ->set('text_results', $form_state->getValue('text_results'))
      ->set('text_delete_item', $form_state->getValue('text_delete_item'))
      ->set('text_delete', $form_state->getValue('text_delete'))
      ->set('text_button', $form_state->getValue('text_button'));

    $config->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * Helper function to get options for enabled themes.
   */
  private function prototype_select_enabled_themes_options() {
    $options = [];

    // Get a list of available themes.
    $themes = $this->themeHandler->listInfo();

    foreach ($themes as $theme_name => $theme) {
      // Only create options for enabled themes.
      if ($theme->status) {
        if (!(isset($theme->info['hidden']) && $theme->info['hidden'])) {
          $options[$theme_name] = $theme->info['name'];
        }
      }
    }

    return $options;
  }

}
