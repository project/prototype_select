<?php

namespace Drupal\prototype_select;

use Drupal\Core\Theme\ThemeManagerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * ThemeOptions returns the user's current section or false if none.
 */
class ThemeOptions {

  /**
   * The devel config.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * The theme handler.
   *
   * @var \Drupal\Core\Theme\ThemeManagerInterface
   */
  protected $themeManager;

  /**
   * Constructs a ThemeInfoRebuildSubscriber object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   The config factory.
   * @param \Drupal\Core\Theme\ThemeManagerInterface $themeManager
   *   The theme handler.
   */
  public function __construct(ConfigFactoryInterface $config, ThemeManagerInterface $themeManager) {
    $this->config = $config;
    $this->themeManager = $themeManager;
  }

  /**
   * Method that returns the user's current site section or false if none.
   */
  public function isThemeEnabled() {
    $activeTheme = $this->themeManager->getActiveTheme()->getName();
    $includedThemes = $this->config->get('prototype_select.settings')->get('prototype_select_include');

    switch ($includedThemes) {
      case PROTOTYPE_SELECT_INCLUDE_EVERYWHERE:
        return TRUE;

      case PROTOTYPE_SELECT_INCLUDE_ADMIN:
        $adminTheme = $this->config->get('system.theme')->get('admin');
        return $activeTheme === $adminTheme;

      case PROTOTYPE_SELECT_INCLUDE_NO_ADMIN:
        $defaultTheme = $this->config->get('system.theme')->get('default');
        return $activeTheme === $defaultTheme;

      default:
        return TRUE;
    }
  }

}
