/**
 * @file
 * Prototype Select behaviors.
 */
import Drupal from 'Drupal';

/**
 * Behavior description.
 */
Drupal.behaviors.prototypeSelect = {
  attach: function(context, settings) {
    let selector = 'select[data-select-a11y]';

    if (settings.prototype_select && settings.prototype_select.selector) {
      selector = settings.prototype_select.selector;
    }

    let selects = Array.from(context.querySelectorAll(selector)).filter(
      element => {
        return !element.matches('.hidden *');
      },
    );

    if (
      settings.prototype_select &&
      settings.prototype_select.exclude_selector
    ) {
      selects = selects.filter(element => {
        return !element.matches(settings.prototype_select.exclude_selector);
      });
    }

    const selectA11ys = Array.prototype.map.call(selects, function(select) {
      new Select(select, settings.prototype_select.options);
    });
  },
};
