/**
 * @file
 * Select A11y
 *
 * This is a customized version of the select-a11y
 * https://gitlab.com/pidila/select-a11y.
 */

const DEFAULT_OPTIONS = {
  searchEnabledThreshold: 5,
  showSelected: true,
  text: {
    help:
      'Use tab (or the down arrow key) to navigate through the list of suggestions',
    placeholder: 'Search in the list',
    noResult: 'No result',
    results: '{x} suggestion(s) available',
    deleteItem: 'Remove {t}',
    delete: 'Remove',
    button: '<label>',
  },
};

const OPTION_ATTRIBUTES = {
  searchEnabledThreshold: 'data-search-threshold',
  showSelected: 'data-show-selected',
  text: {
    help: 'data-text-help',
    placeholder: 'data-text-placeholder',
    noResult: 'data-text-no-result',
    results: 'data-text-results',
    deleteItem: 'data-text-delete-item',
    delete: 'data-text-delete',
    button: 'data-text-button',
  },
};

// Legacy browser support for Element.matches().
const matches =
  Element.prototype.matches ||
  Element.prototype.msMatchesSelector ||
  Element.prototype.webkitMatchesSelector;

let closest = Element.prototype.closest;

if (!closest) {
  closest = function(s) {
    var el = this;

    do {
      if (matches.call(el, s)) return el;
      el = el.parentElement || el.parentNode;
    } while (el !== null && el.nodeType === 1);
    return null;
  };
}

class Select {
  constructor(el, options) {
    this.el = el;
    this.label = document.querySelector(`label[for=${el.id}]`);
    this.id = el.id;
    this.open = false;
    this.multiple = this.el.multiple;
    this.search = '';
    this.suggestions = [];
    this.focusIndex = null;

    // Merge options in the following order of precedence.
    // 1. Data attribute options on the select element
    // 2. Options passed to the constructor
    // 3. Defaults
    const attributeOptions = this._getAttributeOptions();

    const textOptions = {
      ...DEFAULT_OPTIONS.text,
      ...this._sanitizeOptions(options.text),
      ...this._sanitizeOptions(attributeOptions.text),
    };

    this._options = {
      ...DEFAULT_OPTIONS,
      ...this._sanitizeOptions(options),
      ...this._sanitizeOptions(attributeOptions),
      text: textOptions,
    };

    // Bind event handler methods.
    this._handleFocus = this._handleFocus.bind(this);
    this._handleKeyboard = this._handleKeyboard.bind(this);
    this._handleOpener = this._handleOpener.bind(this);
    this._handleReset = this._handleReset.bind(this);
    this._handleSuggestionClick = this._handleSuggestionClick.bind(this);
    this._positionCursor = this._positionCursor.bind(this);
    this._removeOption = this._removeOption.bind(this);

    this._disable();

    // Create markup.
    this.button = this._createButton();
    this.liveZone = this._createLiveZone();
    this.overlay = this._createOverlay();
    this.wrap = this._wrap();

    // Enable multiple value support if needed.
    if (this.multiple && this._options.showSelected) {
      this.selectedList = this._createSelectedList();
      this.selectedListWrapper = this._createSelectedListWrapper();
      this._updateSelectedList();

      this.selectedList.addEventListener('click', this._removeOption);
    }

    // Attach event handlers to DOM elements.
    this.button.addEventListener('click', this._handleOpener);
    this.list.addEventListener('click', this._handleSuggestionClick);
    this.wrap.addEventListener('keydown', this._handleKeyboard);
    if (this.el.form) {
      this.el.form.addEventListener('reset', this._handleReset);
    }
    document.addEventListener('blur', this._handleFocus, true);

    // Bind search events.
    if (this._searchIsEnabled()) {
      this._handleInput = this._handleInput.bind(this);
      this.input.addEventListener('input', this._handleInput);
      this.input.addEventListener('focus', this._positionCursor, true);
    }
  }

  _sanitizeOptions(options) {
    const output = { ...options };
    Object.keys(output).forEach(
      key => output[key] === undefined && delete output[key],
    );
    return output;
  }

  _getAttributeOptions() {
    const el = this.el;
    const getAttributes = attributeMap =>
      function(options, key) {
        let value;

        if (typeof attributeMap[key] === 'object') {
          value = Object.keys(attributeMap[key]).reduce(
            getAttributes(attributeMap[key]),
            {},
          );
        } else {
          if (el.hasAttribute(attributeMap[key])) {
            value = el.getAttribute(attributeMap[key]);
          }
        }

        return Object.assign(options, {
          [key]: value,
        });
      };

    return Object.keys(OPTION_ATTRIBUTES).reduce(
      getAttributes(OPTION_ATTRIBUTES),
      {},
    );
  }

  _searchIsEnabled() {
    const searchEnabledThreshold = parseInt(
      this._options.searchEnabledThreshold,
      10,
    );
    if (searchEnabledThreshold === 0) {
      return false;
    }

    return this.el.options.length >= searchEnabledThreshold;
  }

  _createButton() {
    const button = document.createElement('button');
    button.setAttribute('type', 'button');
    button.setAttribute('aria-expanded', this.open);
    button.className = 'c-select-a11y__button';

    const text = document.createElement('span');
    text.classList.add('c-select-a11y__value');

    if (this.multiple) {
      if (!this._options.text.button || this._options.text.button == '<label>') {
        text.innerText = this.label.innerText;
      }
      else {
        text.innerText = this._options.text.button;
      }
    } else {
      const selectedOption = this.el.item(this.el.selectedIndex);
      text.innerText = selectedOption.label || selectedOption.value;

      if (!this.label.id) {
        this.label.id = `${this.el.id}-label`;
      }
      button.setAttribute('id', this.el.id + '-button');
      button.setAttribute('aria-labelledby', this.label.id + ' ' + button.id);
    }

    button.appendChild(text);

    button.insertAdjacentHTML(
      'beforeend',
      '<span class="c-select-a11y__icon--select" aria-hidden="true"></span>',
    );

    return button;
  }

  _createLiveZone() {
    const live = document.createElement('p');
    live.setAttribute('aria-live', 'polite');
    live.classList.add('visually-hidden');

    return live;
  }

  _createOverlay() {
    const container = document.createElement('div');
    container.classList.add('c-select-a11y__container');

    const suggestions = document.createElement('div');
    suggestions.classList.add('c-select-a11y__suggestions');
    suggestions.id = `a11y-${this.id}-suggestions`;

    const inputHtml = this._searchIsEnabled()
      ? `<input type="text" id="a11y-${this.id}-js" class="${this.el.className} c-select-a11y__search" autocomplete="off" autocapitalize="off" spellcheck="false" placeholder="${this._options.text.placeholder}" aria-describedby="a11y-usage-${this.id}-js">`
      : '';

    container.innerHTML = `
      <p id="a11y-usage-${this.id}-js" class="visually-hidden">${this._options.text.help}</p>
      <label for="a11y-${this.id}-js" class="visually-hidden">${this._options.text.placeholder}</label>
      ${inputHtml}
    `;

    container.appendChild(suggestions);

    this.list = suggestions;
    this.input = container.querySelector('input');

    return container;
  }

  _createSelectedList() {
    const list = document.createElement('ul');
    list.className = 'c-select-a11y__chips';
    list.innerHTML;

    return list;
  }

  _createSelectedListWrapper() {
    const wrapper = document.createElement('div');
    wrapper.className = 'c-select-a11y__chips-wrapper';
    wrapper.appendChild(this.selectedList);

    return wrapper;
  }

  _disable() {
    this.el.setAttribute('tabindex', -1);
  }

  _fillSuggestions() {
    const search = this.search.toLowerCase();
    const suggestions = [];

    // loop over direct children
    //  if an option, add option
    // count valid options, sum group count when inserted
    //  if a group
    // recurse of optgroups
    // If not empty, return a rendered list
    // Make a top level ul.
    // opt groups will be nested ul
    // only insert an opt group if not empty
    // options are a li

    /**
     * Create a group out of options.
     *
     * @param {Element} groupElement
     *  The root <select> element or <optgroup> subelement
     * @return {Element|Boolean}
     *  A <ul> of options or false if empty.
     */
    function createGroup(groupElement, startingIndex) {
      const items = Array.from(groupElement.children)
        .map(createOption)
        .filter(Boolean);

      if (items.length <= 0) {
        return false;
      }

      const group = document.createElement('ul');
      group.setAttribute('role', 'group');
      group.setAttribute('disabled', groupElement.getAttribute('disabled'));
      group.classList.add('c-select-a11y__group');

      const labelText = groupElement.getAttribute('label');
      if (labelText) {
        const label = document.createElement('li');
        label.setAttribute('role', 'presentation');
        label.classList.add('c-select-a11y__group-label');
        label.innerText = labelText;
        group.appendChild(label);
      }

      items.forEach(item => group.appendChild(item));

      return group;
    }

    function createOption(option, index) {
      // Handle option groups. a.k.a <optgroup>
      if (option.tagName.toUpperCase() === 'OPTGROUP') {
        return createGroup(option);
      }

      const text = option.label || option.value;
      const formatedText = text.toLowerCase();

      // test if search text match the current option
      if (formatedText.indexOf(search) === -1) {
        return;
      }

      // create the option
      const suggestion = document.createElement('li');
      suggestion.setAttribute('role', 'option');
      suggestion.setAttribute('tabindex', 0);
      suggestion.setAttribute('data-index', option.index);
      suggestion.classList.add('c-select-a11y__suggestion');

      // check if the option is selected
      if (option.selected) {
        suggestion.setAttribute('aria-selected', 'true');
      }

      suggestion.innerText = option.label || option.value;

      suggestions.push(suggestion);

      return suggestion;
    }

    const suggestionsRoot = createGroup(this.el, 0);
    this.suggestions = suggestions;

    if (!this.suggestions) {
      this.list.innerHTML = `<p class="c-select-a11y__no-suggestion">${this._options.text.noResult}</p>`;
    } else {
      const listBox = document.createElement('div');
      listBox.setAttribute('role', 'listbox');

      if (this.multiple) {
        listBox.setAttribute('aria-multiselectable', 'true');
      }

      listBox.appendChild(suggestionsRoot);

      this.list.innerHTML = '';
      this.list.appendChild(listBox);
    }

    this._setLiveZone();
  }

  _handleOpener(event) {
    this._toggleOverlay();
  }

  _handleFocus() {
    if (!this.open) {
      return;
    }

    clearTimeout(this._focusTimeout);

    this._focusTimeout = setTimeout(
      function() {
        if (
          !this.overlay.contains(document.activeElement) &&
          this.button !== document.activeElement
        ) {
          this._toggleOverlay(false, document.activeElement === document.body);
        } else if (document.activeElement === this.input) {
          // reset the focus index
          this.focusIndex = null;
        } else {
          const optionIndex = this.suggestions.indexOf(document.activeElement);

          if (optionIndex !== -1) {
            this.focusIndex = optionIndex;
          }
        }
      }.bind(this),
      10,
    );
  }

  _handleReset() {
    clearTimeout(this._resetTimeout);

    this._resetTimeout = setTimeout(
      function() {
        this._fillSuggestions();
        if (this.multiple && this._options.showSelected) {
          this._updateSelectedList();
        } else if (!this.multiple) {
          const option = this.el.item(this.el.selectedIndex);
          this._setButtonText(option.label || option.value);
        }
      }.bind(this),
      10,
    );
  }

  _handleSuggestionClick(event) {
    const option = closest.call(event.target, '[role="option"]');

    if (!option) {
      return;
    }

    const optionIndex = parseInt(option.getAttribute('data-index'), 10);
    const shouldClose = this.multiple && event.metaKey ? false : true;

    this._toggleSelection(optionIndex, shouldClose);
  }

  _handleInput() {
    // prevent event fireing on focus and blur
    if (this.search === this.input.value) {
      return;
    }

    this.search = this.input.value;
    this._fillSuggestions();
  }

  _handleKeyboard(event) {
    const option = closest.call(event.target, '[role="option"]');
    const input = closest.call(event.target, 'input');

    if (event.keyCode === 27) {
      this._toggleOverlay();
      return;
    }

    if (input && event.keyCode === 13) {
      event.preventDefault();
      return;
    }

    if (event.keyCode === 40) {
      event.preventDefault();
      this._moveIndex(1);
      return;
    }

    if (!option) {
      return;
    }

    if (event.keyCode === 39) {
      event.preventDefault();
      this._moveIndex(1);
      return;
    }

    if (event.keyCode === 37 || event.keyCode === 38) {
      event.preventDefault();
      this._moveIndex(-1);
      return;
    }

    if ((!this.multiple && event.keyCode === 13) || event.keyCode === 32) {
      event.preventDefault();
      this._toggleSelection(
        parseInt(option.getAttribute('data-index'), 10),
        this.multiple ? false : true,
      );
    }

    if (this.multiple && event.keyCode === 13) {
      this._toggleOverlay();
    }
  }

  _moveIndex(step) {
    if (this.focusIndex === null) {
      this.focusIndex = 0;
    } else {
      const nextIndex = this.focusIndex + step;
      const selectionItems = this.suggestions.length - 1;

      if (nextIndex > selectionItems) {
        this.focusIndex = 0;
      } else if (nextIndex < 0) {
        this.focusIndex = selectionItems;
      } else {
        this.focusIndex = nextIndex;
      }
    }

    this.suggestions[this.focusIndex].focus();
  }

  _positionCursor() {
    setTimeout(
      function() {
        this.input.selectionStart = this.input.selectionEnd = this.input.value.length;
      }.bind(this),
    );
  }

  _removeOption(event) {
    const button = closest.call(event.target, 'button');

    if (!button) {
      return;
    }

    const currentButtons = this.selectedList.querySelectorAll('button');
    const buttonPreviousIndex =
      Array.prototype.indexOf.call(currentButtons, button) - 1;
    const optionIndex = parseInt(button.getAttribute('data-index'), 10);

    // disable the option
    this._toggleSelection(optionIndex);

    // manage the focus if there's still the selected list
    if (this.selectedList.parentElement) {
      const buttons = this.selectedList.querySelectorAll('button');

      // look for the button before the one clicked
      if (buttons[buttonPreviousIndex]) {
        buttons[buttonPreviousIndex].focus();
      }
      // fallback to the first button in the list if there's none
      else {
        buttons[0].focus();
      }
    } else {
      this.button.focus();
    }
  }

  _setButtonText(text) {
    this.button.firstElementChild.innerText = text;
  }

  _setLiveZone() {
    const suggestions = this.suggestions.length;
    let text = '';

    if (this.open) {
      if (!suggestions) {
        text = this._options.text.noResult;
      } else {
        text = this._options.text.results.replace('{x}', suggestions);
      }
    }

    this.liveZone.innerText = text;
  }

  _setFocusOnOpen() {
    if (this._searchIsEnabled()) {
      this.input.focus();
    } else if (this.suggestions.length > 0) {
      this.suggestions[0].focus();
    }
  }

  _toggleOverlay(state, focusBack) {
    this.open = state !== undefined ? state : !this.open;
    this.button.setAttribute('aria-expanded', this.open);

    if (this.open) {
      this._fillSuggestions();
      this.button.insertAdjacentElement('afterend', this.overlay);
      this._setFocusOnOpen();
    } else if (this.wrap.contains(this.overlay)) {
      this.wrap.removeChild(this.overlay);

      // reset the focus index
      this.focusIndex = null;

      // reset search values
      if (this._searchIsEnabled()) {
        this.input.value = '';
        this.search = '';
      }

      // reset aria-live
      this._setLiveZone();
      if (state === undefined || focusBack) {
        // fix bug that will trigger a click on the button when focusing directly
        setTimeout(
          function() {
            this.button.focus();
          }.bind(this),
        );
      }
    }
  }

  _toggleSelection(optionIndex, close = true) {
    const option = this.el.item(optionIndex);

    if (this.multiple) {
      this.el.item(optionIndex).selected = !this.el.item(optionIndex).selected;
    } else {
      this.el.selectedIndex = optionIndex;
    }

    this.suggestions.forEach(
      function(suggestion) {
        const index = parseInt(suggestion.getAttribute('data-index'), 10);

        if (this.el.item(index).selected) {
          suggestion.setAttribute('aria-selected', 'true');
        } else {
          suggestion.removeAttribute('aria-selected');
        }
      }.bind(this),
    );

    if (!this.multiple) {
      this._setButtonText(option.label || option.value);
    } else if (this._options.showSelected) {
      this._updateSelectedList();
    }

    if (close && this.open) {
      this._toggleOverlay();
    }

    this.el.dispatchEvent(new Event('change'));
  }

  _updateSelectedList() {
    const items = Array.prototype.map
      .call(
        this.el.options,
        function(option, index) {
          if (!option.selected) {
            return;
          }

          const text = option.label || option.value;

          return `
        <li class="c-select-a11y__chip">
          <span>${text}</span>
          <button class="c-select-a11y__chip-button" title="${this._options.text.deleteItem.replace(
            '{t}',
            text,
          )}" type="button" data-index="${index}">
            <span class="visually-hidden">${this._options.text.delete}</span>
            <span class="c-select-a11y__chip-icon" aria-hidden="true"></span>
          </button>
        </li>`;
        }.bind(this),
      )
      .filter(Boolean);

    this.selectedList.innerHTML = items.join('');

    if (items.length) {
      if (!this.selectedListWrapper.parentElement) {
        this.wrap.appendChild(this.selectedListWrapper);
      }
    } else if (this.selectedListWrapper.parentElement) {
      this.wrap.removeChild(this.selectedListWrapper);
    }
  }

  _wrap() {
    const wrapper = document.createElement('div');
    wrapper.classList.add('c-select-a11y');
    this.el.parentElement.appendChild(wrapper);

    const tagHidden = document.createElement('div');
    tagHidden.classList.add('hidden');
    tagHidden.setAttribute('aria-hidden', true);

    if (this.multiple) {
      // tagHidden.appendChild(this.label);
    }
    tagHidden.appendChild(this.el);

    wrapper.appendChild(tagHidden);
    wrapper.appendChild(this.liveZone);
    wrapper.appendChild(this.button);

    return wrapper;
  }
}

export default Select;
