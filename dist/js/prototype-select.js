(function (Drupal) {
  'use strict';

  function _interopDefaultLegacy (e) { return e && typeof e === 'object' && 'default' in e ? e : { 'default': e }; }

  var Drupal__default = /*#__PURE__*/_interopDefaultLegacy(Drupal);

  /**
   * @file
   * Prototype Select behaviors.
   */

  /**
   * Behavior description.
   */
  Drupal__default['default'].behaviors.prototypeSelect = {
    attach: function(context, settings) {
      let selector = 'select[data-select-a11y]';

      if (settings.prototype_select && settings.prototype_select.selector) {
        selector = settings.prototype_select.selector;
      }

      let selects = Array.from(context.querySelectorAll(selector)).filter(
        element => {
          return !element.matches('.hidden *');
        },
      );

      if (
        settings.prototype_select &&
        settings.prototype_select.exclude_selector
      ) {
        selects = selects.filter(element => {
          return !element.matches(settings.prototype_select.exclude_selector);
        });
      }

      Array.prototype.map.call(selects, function(select) {
        new Select(select, settings.prototype_select.options);
      });
    },
  };

}(Drupal));
