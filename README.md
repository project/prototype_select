# Prototype Select

This module provides enhancements to `<select>` elements, including an accessible combobox for multi-select elements and search capabilities for long list. It also makes simple select elements stylable with CSS.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/prototype_select).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/prototype_select).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

None.


## Installation

Install as you would normally install a contributed Drupal module. For further information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Enable the module at Administration > Extend.
1. Configuration settings can be found `/admin/config/user-interface/prototype_select`.


## Maintainers

- John Ferris - [pixelwhip](https://www.drupal.org/u/pixelwhip)